#!/bin/bash
# originally by tomahawk47@gmail.com
# required: mplayer
# moded by https://github.com/phm3

# ADDED: external confing for streams
# extra if for mac Os and brew install of mplayer
# TODO: play by ID
clear
echo "################################################"
echo "### Kill with CTRL+C ###########################"
echo "################################################"

if  find /usr/bin/mplayer >/dev/null 2>&1

then 

	echo "Searching for Mplayer                 [OK]"
	# if mplayer is shipped differently eg. brew on Mac OS 
elif mplayer >/dev/null 2>&1 
then
	echo "Searching for Mplayer          [OK]"
else
	echo "Searching for Mplayer        [Failed]"
	echo "Mplayer is required to start the script"
	exit
fi

if ping -c 1 ask.com >/dev/null 2>&1
then
	echo "Internet                  [OK]"
else
	echo "Internet                [Failed]"
	echo "No Internet connection"
	exit
fi

filename="streams.conf"
re='^[0-9]+$'


echo "Select stream:"
while read -r line
do

	name=$line

	# split the line

	id=`echo $name | awk -F'|' '{print $1}'`
	nickname=`echo $name | awk -F'|' '{print $2}'`
	url=`echo $name | awk -F'|' '{print $3}'`



	if [ $# -eq 0 ]
	then


		echo "ALIAS: ("$nickname") ID: ("$id")";

	fi


done < "$filename"
echo "Enter the alias:"
read address

echo "Playing...": $address


if  [[ $address =~ $re ]] ; then
  
   stream=`cat $filename | grep $address"|" | awk -F '|'  '{print $3}'`;
else
	stream=`cat $filename | grep $address | awk -F '|'  '{print $3}'`;
fi


# set title
title=`cat $filename | grep $address | awk -F '|'  '{print $2}'`;
echo -e '\033]2;'Playing $title »  $stream'\007'


#play
mplayer $stream;

exit;



